

|                         Critères                          | Cookie | Token | JWT | Macaron |
|:---------------------------------------------------------:|:------:|:-----:|:---:|:-------:|
|                         Sécurisé                          |   ❌    |   ✅   |  ✅  |    ✅    |
|                     Payload normalisé                     |   ❌    |   ❌   |  ❌  |    ✅    |
|                         Immutable                         |   ❌    |   ❌   |  ❌  |    ✅    |
|                 Vérifié par clef publique                 |   ❌    |   ✅   |  ❌  |    ❌    |
| Peut contenir des données non utiles pour la vérification |   ❌    |   ✅   |  ✅  |    ❌    |



|                         Critères                          | Cookie | Token | JWT | Macaron | Biscuit |
|:---------------------------------------------------------:|:------:|:-----:|:---:|:-------:|:-------:|
|                         Sécurisé                          |   ❌    |   ✅   |  ✅  |    ✅    |    ❔    |
|                     Payload normalisé                     |   ❌    |   ❌   |  ❌  |    ✅    |    ❔    |
|                  Vérification normalisée                  |   ❌    |   ❌   |  ❌  |    ✅    |    ❔    |
|                         Immuable                          |   ❌    |   ❌   |  ❌  |    ✅    |    ❔    |
|                 Vérifié par clef publique                 |   ❌    |   ✅   |  ✅  |    ❌    |    ❔    |
| Peut contenir des données non utiles pour la vérification |   ✅    |   ✅   |  ✅  |    ❌    |    ❔    |