use biscuit_auth::builder::{BlockBuilder, Check, Fact, Rule};
use biscuit_auth::macros::{authorizer, block};
use biscuit_auth::{error, Biscuit, KeyPair};

fn main() {
    let keypair = KeyPair::new();

    let biscuit: Biscuit = create_token(&keypair).expect("Unable to create token");

    let attenuate_biscuit: Biscuit = attenuate(&biscuit).expect("Unable to attenuate biscuit");

    // dbg!(&keypair);
    // dbg!(&biscuit);
    // dbg!(&attenuate_biscuit);
    //
    let b64 = biscuit.to_base64().expect("Unable to serialize");

    //dbg!(&b64);

    let pubkey = keypair.public().to_bytes();

    //dbg!(keypair.public().to_bytes());

    // dbg!(Biscuit::from_base64(b64, keypair.public()).expect(""));

    //dbg!(&attenuate_biscuit);

    let r = authorize(&attenuate_biscuit);

    if let Err(e) = r {
        println!("{e:?}");

        if let Some(e) = e.downcast_ref::<biscuit_auth::error::Token>() {
            dbg!(e);
        }
    }
}

fn create_token(root: &KeyPair) -> Result<Biscuit, error::Token> {
    let mut builder = Biscuit::builder();

    let mut user = Fact::try_from("user({user})")?;
    user.set("user", 667)?;

    // let mut user2 = Fact::try_from("user2({user})")?;
    // user2.set("user", 667)?;
    //
    // let mut org_id = Fact::try_from("org_id({org_id})")?;
    // org_id.set("org_id", "org_id_1252555")?;
    //
    builder.add_fact(user)?;
    // builder.add_fact(user2)?;
    // builder.add_fact(org_id)?;
    //
    // builder.set_root_key_id(42);

    builder.build(&root)
}

fn attenuate(token: &Biscuit) -> eyre::Result<Biscuit> {
    let block = block! {
        r#"
            check if resource($resource), $resource.starts_with({data})
        "#,
        data = "data"
    };

    // builder.add_check(r#"check if resource($resource), $resource.starts_with("data")"#)?;
    // builder.add_fact(r#"user3(668)"#)?;

    Ok(token.append(block)?)
}

fn authorize_macro(token: &Biscuit) -> eyre::Result<()> {
    let aut = authorizer!(
        r#"
     operation({operation});
     resource({resource});

     is_allowed($user_id) <- right($user_id, $resource, $operation),
                             resource($resource),
                             operation($operation);

     allow if is_allowed({user_id});
  "#,
        operation = "read",
        resource = "file1",
        user_id = "1234",
    );

    Ok(())
}

fn authorize(token: &Biscuit) -> eyre::Result<()> {
    let mut authorizer = token.authorizer()?;

    let user_id: Vec<(i64,)> = authorizer.query("_($user_id) <- user($user_id)")?;
    dbg!(user_id);

    authorizer.set_time();

    let mut resource = Fact::try_from("resource({resource})")?;
    resource.set("resource", "resource1/*")?;

    let mut resource2 = Fact::try_from("resource({resource})")?;
    resource2.set("resource", "resource2/*")?;

    let mut resource3 = Fact::try_from("resource({resource})")?;
    resource3.set("resource", "data1/*")?;

    let mut resource2 = Fact::try_from("resource({resource})")?;
    resource2.set("resource", "resource2/*")?;

    let mut operation: Fact = "operation({operation})".try_into()?;
    operation.set("operation", "write")?;

    let mut acl: Fact = "right({user}, {resource}, {operation})".try_into()?;
    acl.set("user", 666)?;
    acl.set("resource", "resource1/*")?;
    acl.set("operation", "write")?;

    let mut acl2: Fact = "right({user}, {resource}, {operation})".try_into()?;
    acl2.set("user", 668)?;
    acl2.set("resource", "resource1/*")?;
    acl2.set("operation", "write")?;

    let is_allowed: Rule = r#"is_allowed($user, $resource, $operation) <-
        user($user),
        resource($resource),
        operation($operation),
        right($user, $resource, $operation)
    "#
    .try_into()?;

    let check_is_allowed = Check::try_from(is_allowed)?;

    authorizer.add_fact(resource)?;
    authorizer.add_fact(resource2)?;
    //authorizer.add_fact(resource3)?;
    authorizer.add_fact(operation)?;
    authorizer.add_fact(acl)?;
    authorizer.add_fact(acl2)?;
    authorizer.add_check(check_is_allowed)?;

    let world = authorizer.print_world();
    //println!("{world}");

    authorizer.allow()?;

    authorizer.authorize()?;

    let get_user_data = Rule::try_from(
        r#"
            _($user_id, $org_id, $resource) <-
            org_id($org_id),
            user($user_id),
            resource($resource)
        "#,
    )?;

    let res: Vec<(i64, String, String)> = authorizer.query_all(get_user_data).unwrap();

    dbg!(res);

    Ok(())
}
